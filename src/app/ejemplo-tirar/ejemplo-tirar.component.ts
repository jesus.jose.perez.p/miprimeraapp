import { Component, OnInit } from '@angular/core';
// se genera asi ng g c ejemplo-tirar -s -t
// esto es para generarlo con template y styles inline
// esto no lo recomiendo por una estructura mejor y mas clara
@Component({
  selector: 'app-ejemplo-tirar',
  template: `
    <p>
      ejemplo-tirar works!
    </p>
  `,
  styles: [
  ]
})
export class EjemploTirarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
