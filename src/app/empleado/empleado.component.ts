import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html', // se podria eliminar ya que es muy simple 
  //template: '<p>Aquí iría un empleado</p>',
  styleUrls: ['./empleado.component.css']   // se podria eliminar ya que es muy simple
  //styles: ['p { background-color: red; }']
})
export class EmpleadoComponent implements OnInit {

  nombre = "Jesús";
  apellido = "Pérez"
  edad = 14;
  empresa = "Agrícola Pérez";

  /*getEdad(){
    return this.edad;
  }*/

  constructor() { }

  ngOnInit(): void {
  }

}
